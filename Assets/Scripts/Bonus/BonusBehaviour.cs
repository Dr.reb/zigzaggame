﻿using UnityEngine;
using Zenject;

/// <summary>
/// Базовый класс для бонусов
/// </summary>
public abstract class BonusBehaviour : MonoBehaviour
{
    protected virtual void Update()
    {
        Animation();
    }

    protected virtual void Animation()
    {
        transform.Rotate(new Vector3(0, 45, 0) * Time.deltaTime);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            CollisionWithPlayer();
        }
    }

    protected virtual void CollisionWithPlayer()
    {

    }
    
    public class Factory : PlaceholderFactory<BonusType ,BonusBehaviour>
    {
    }
}


public enum BonusType
{
    Standart
}