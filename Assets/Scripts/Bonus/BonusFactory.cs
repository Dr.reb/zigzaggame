﻿using System;
using UnityEngine;
using Zenject;

public class BonusFactory : IFactory<BonusType ,BonusBehaviour>
{
    private readonly DiContainer _container;
    private readonly Settings _settings;

    public BonusFactory(DiContainer container, Settings settings)
    {
        _container = container;
        _settings = settings;
    }

    public BonusBehaviour Create(BonusType type)
    {
        switch (type)
        {
            default:
                throw new ArgumentException("Неопознанный тип бонуса " + type.ToString());
            case BonusType.Standart:
                return _container.InstantiatePrefabForComponent<BonusBehaviour>(_settings.StandartBonusPrefab);

        }
            
    }

    [Serializable]
    public class Settings
    {
        public GameObject StandartBonusPrefab;
    }

}