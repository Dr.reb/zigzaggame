﻿using UnityEngine;
using System;
using Zenject;

public class BonusStandart : BonusBehaviour
{
    private Settings _settings;
    private ScoreSystem _scoreSystem;
    private AudioSystem _audioPlayer;

    private const float _particleLifetime = 2f;

    [Inject]
    private void Construct(Settings settings, ScoreSystem scoreSystem, AudioSystem player)
    {
        _settings = settings;
        _scoreSystem = scoreSystem;
        _audioPlayer = player;
    }


    protected override void CollisionWithPlayer()
    {
        _scoreSystem.IncreaseScore();

        _audioPlayer.Play(_settings.BonusCollectedSound);

        GameObject particles = Instantiate(_settings.BonusCollectedParticles);
        particles.transform.position = this.transform.position;
        Destroy(particles, _particleLifetime);

        Destroy(this.gameObject);
    }

    [Serializable]
    public class Settings
    {
        public AudioClip BonusCollectedSound;
        public GameObject BonusCollectedParticles;
    }
}
