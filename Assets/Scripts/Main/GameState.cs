﻿using Zenject;


/// <summary>
/// Базовый класс состояний игры.
/// </summary>
public abstract class GameState
{
    protected InputHandler _input;
    protected ZigZagGameController _gameController;

    public GameState(InputHandler input, ZigZagGameController controller)
    {
        _input = input;
        _gameController = controller;
    }
    
    public abstract void StartState();
    public abstract void UpdateState();
    public abstract void FinishState();

    public class Factory : PlaceholderFactory<GameStates, GameState>
    {

    }
}
