﻿public class GameStateWaiting : GameState
{
    private readonly Player _player;
    private readonly LevelManager _levelManager;
    
    public GameStateWaiting(LevelManager manager, Player player, ZigZagGameController gameController, InputHandler input) : base(input, gameController)
    {
        _levelManager = manager;
        _player = player;
    }

    public override void StartState()
    {
        _levelManager.ResetLevel();
        _player.ResetPlayer();
    }

    public override void UpdateState()
    {
        if (_input.Tap())
        {
            _gameController.SetState(GameStates.Playing);
        }
    }

    public override void FinishState()
    {

    }
}
