﻿public class GameStatePlaying : GameState
{
    private readonly Player _player;
    private readonly LevelManager _levelManager;
    private readonly AudioSystem _audioSystem;


    public GameStatePlaying(LevelManager manager, Player player, ZigZagGameController gameController, InputHandler input, AudioSystem audioSystem) : base(input, gameController)
    {
        _levelManager = manager;
        _player = player;
        _audioSystem = audioSystem;
    }

    public override void StartState()
    {
        _levelManager.SpawnTiles(100);
        _audioSystem.PlayMusic();
    }

    public override void UpdateState()
    {
        _levelManager.UpdateLevel();

        _player.UpdatePlayer(_input.Tap());

        if (_player.IsPlayerFell)
            _gameController.SetState(GameStates.GameOver);
    }

    public override void FinishState()
    {

    }
}
