﻿public class GameStateOver : GameState
{
    private readonly AudioSystem _audioSystem;
    private readonly Player _player;

    public GameStateOver(AudioSystem audioSystem, ZigZagGameController gameController, InputHandler input, Player player) : base(input, gameController)
    {
        _audioSystem = audioSystem;
        _player = player;
    }

    public override void StartState()
    {
        _audioSystem.StopMusic();
        _player.PlayFailSound();
    }

    public override void UpdateState()
    {
        if (_input.Tap())
        {
            _gameController.SetState(GameStates.WaitingToStart);
        }
    }

    public override void FinishState()
    {

    }
}
