﻿using Zenject;

/// <summary>
/// Машина состояний игры, обрабатывает различные игровые состояния.
/// </summary>
public class ZigZagGameController :ITickable, IInitializable
{
    private readonly SignalBus _signalBus;
    private readonly GameState.Factory _stateFactory;

    private GameState _currentState;

    public ZigZagGameController(SignalBus bus, GameState.Factory stateFactory)
    {
        _signalBus = bus;
        _stateFactory = stateFactory;
    }

    public void Initialize()
    {
        SetState(GameStates.WaitingToStart);
    }

    public void SetState(GameStates state)
    {
        if(_currentState != null)
            _currentState.FinishState();

        _currentState = _stateFactory.Create(state);

        _currentState.StartState();
        _signalBus.Fire(new GameStateChanged { CurrentState = state });
    }

    public void Tick()
    {
        _currentState.UpdateState();
    }

}

public class GameStateChanged
{
    public GameStates CurrentState;
}

public enum GameStates
{
    WaitingToStart,
    Playing,
    GameOver
}