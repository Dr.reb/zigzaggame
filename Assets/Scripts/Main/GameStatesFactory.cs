﻿using System;
using Zenject;

public class GameStatesFactory : IFactory<GameStates, GameState>
{
    private readonly DiContainer _container;

    public GameStatesFactory(DiContainer container)
    {
        _container = container;
    }

    public GameState Create(GameStates state)
    {
        switch (state)
        {
            default:
                throw new ArgumentException("Неизвестное состояние " + state.ToString());
            case GameStates.Playing:
                return _container.Instantiate<GameStatePlaying>();
            case GameStates.GameOver:
                return _container.Instantiate<GameStateOver>();
            case GameStates.WaitingToStart:
                return _container.Instantiate<GameStateWaiting>();

        }
    }



}
