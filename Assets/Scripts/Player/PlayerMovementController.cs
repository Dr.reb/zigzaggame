﻿using System;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    private Vector3 _currentDirection = Vector3.forward;
    private Vector3 _startPoint = Vector3.zero;
    
    private void Awake()
    {
        _startPoint = transform.position;
    }

    public void ResetPlayer()
    {
        _currentDirection = Vector3.forward;
        transform.position = _startPoint;
    }

    public void ChangeDir()
    {
        _currentDirection = _currentDirection == Vector3.forward ? Vector3.right : Vector3.forward;
    }

    public void Move(float speed)
    {
        transform.position += _currentDirection * Time.deltaTime * speed;
    }
    
}
