﻿using UnityEngine;

public class PlayerConditionChecker
{
    private readonly Transform _playerTransform;

    public bool IsFell
    {
        get { return _playerTransform.position.y < 0; }
    }

    public PlayerConditionChecker(PlayerMovementController controller)
    {
        _playerTransform = controller.transform;
    }
}
