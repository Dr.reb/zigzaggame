﻿using System;
using UnityEngine;
using Zenject;

public class Player
{
    private readonly PlayerConditionChecker _condition;
    private readonly PlayerMovementController _controller;
    private readonly AudioSystem _audioSystem;
    private readonly Settings _settings;

    [Inject]
    public Player(PlayerConditionChecker conditionChecker, PlayerMovementController controller, AudioSystem audio, Settings settings)
    {
        _condition = conditionChecker;
        _controller = controller;
        _audioSystem = audio;
        _settings = settings;
    }

    public bool IsPlayerFell
    {
        get;
        private set;
    }

    public void PlayFailSound()
    {
        _audioSystem.Play(_settings.FailSound);
    }

    public void UpdatePlayer(bool changeDir)
    {
        if (changeDir)
            _controller.ChangeDir();
        _controller.Move(_settings.Speed);
        IsPlayerFell = _condition.IsFell;
    }

    public void ResetPlayer()
    {
        _controller.ResetPlayer();
    }
    
    [Serializable]
    public class Settings
    {
        public float Speed;
        public AudioClip FailSound;
    }

}
