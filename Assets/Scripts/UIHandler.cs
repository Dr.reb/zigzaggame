﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIHandler : MonoBehaviour
{
    [SerializeField]
    private Text _scoreText;

    [SerializeField]
    private Text _finishScoreText;

    [SerializeField]
    private GameObject _startScreen;

    [SerializeField]
    private GameObject _endScreen;

    private GameStates _currentState;
    private ScoreSystem _scoreSystem;

    [Inject]
    private void Construct(SignalBus bus, ScoreSystem scoreSystem)
    {
        _scoreSystem = scoreSystem;
        bus.Subscribe<GameStateChanged>(OnGameStateChanged);
    }

    public void Start()
    {
        _scoreSystem.ScoreChanged += OnScoreChanged;
    }

    public void OnScoreChanged(int score)
    {
        _scoreText.text = score.ToString();
    }

    private void OnGameStateChanged(GameStateChanged args)
    {
        SetUIState(args.CurrentState);
    }

    private void SetUIState(GameStates state)
    {
        LeaveState(_currentState);
        switch (state)
        {
            case GameStates.GameOver:
                _endScreen.SetActive(true);
                _finishScoreText.text = _scoreText.text;
                break;
            case GameStates.Playing:
                _scoreText.gameObject.SetActive(true);
                break;
            case GameStates.WaitingToStart:
                _startScreen.SetActive(true);
                break;
        }
        _currentState = state;
    }

    private void LeaveState(GameStates state)
    {
        switch (state)
        {
            case GameStates.GameOver:
                _endScreen.SetActive(false);
                break;
            case GameStates.Playing:
                _scoreText.gameObject.SetActive(false);
                break;
            case GameStates.WaitingToStart:
                _startScreen.SetActive(false);
                break;
        }
    }
}
