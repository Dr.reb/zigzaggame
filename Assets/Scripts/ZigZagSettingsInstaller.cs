using UnityEngine;
using Zenject;
using System;

[CreateAssetMenu(fileName = "SettingsInstaller", menuName = "Installers/SettingsInstaller")]
public class ZigZagSettingsInstaller : ScriptableObjectInstaller<ZigZagSettingsInstaller>
{
    public BonusFactory.Settings BonusFactory;
    public Player.Settings PlayerSettings;
    public ZigZagGameInstaller.Settings set;
    public BonusSettings _BonusSettings;


    public override void InstallBindings()
    {
        Container.BindInstance(PlayerSettings);
        Container.BindInstance(BonusFactory);
        Container.BindInstance(set);
        Container.BindInstance(_BonusSettings.StandartBonusSettings);
    }

    [Serializable]
    public class BonusSettings
    {
        public BonusStandart.Settings StandartBonusSettings;
    }
}