﻿using System;


/// <summary>
/// Система подсчёта очков
/// </summary>
public class ScoreSystem 
{
    private int _currentScore;

    public int Score
    {
        get { return _currentScore; }
        private set
        {
            _currentScore = value;
            ScoreChanged.Invoke(value);
        }
    }

    public event Action<int> ScoreChanged;


    public void IncreaseScore()
    {
        Score++;
    }

    /// <summary>
    /// Подписывается на сигнал смены состояния. Подпись происходит в ZigZagGameInstaller
    /// </summary>
    public void ResetScore(GameStateChanged args)
    {
        if(args.CurrentState == GameStates.Playing)
        {
            Score = 0;
        }
    }
}
