﻿using UnityEngine;
using Zenject;

public class CameraMover : MonoBehaviour
{
    private Vector3 _offset;

    private Transform _playerTransform;

    [Inject]
    private void Construct(PlayerMovementController player)
    {
        _playerTransform = player.transform;
        _offset = transform.position - _playerTransform.position;
    }
    
    private void LateUpdate()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {
        Vector3 temp = _playerTransform.position + _offset;
        temp.y = this.transform.position.y;
        transform.position = temp;
    }
}
