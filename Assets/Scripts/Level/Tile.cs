﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Zenject;
using DG.Tweening;

public class Tile : MonoBehaviour
{
    [Inject]
    private Pool _pool;

    private BonusBehaviour _bonus;

    private const float _timeBeforeAnimation = 0.5f; //Время в секундах перед началом анимации падения тайлов.


    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
            TileWasWalked();
    }


    /// <summary>
    /// Прикрепляет бонус к тайлу
    /// </summary>
    public void SetBonus(BonusBehaviour bonus)
    {
        _bonus = bonus;
    }


    /// <summary>
    /// Поиводит тайл к изначальному виду - удаляет бонус и останавливает анимацию DotWeen'а, возвращает стандартный размер.
    /// </summary>
    public void ClearTile()
    {
        transform.DOKill();
        transform.localScale = Vector3.one;
        if (_bonus != null)
            Destroy(_bonus.gameObject);
    }

    private void TileWasWalked()
    {
        StartCoroutine(AnimationCoroutine());
    }

    private IEnumerator AnimationCoroutine()
    {
        yield return new WaitForSeconds(_timeBeforeAnimation);
        Animation();
    }

    private void Animation()
    {
        this.transform.DOMoveY(-5, 2).OnComplete(Despawn);
    }

    public void Despawn()
    {
        _pool.Despawn(this);
    }



    public class Pool : MonoMemoryPool<Tile>
    {
        private List<Tile> _spawnedList = new(100);

        protected override void OnSpawned(Tile item)
        {
            _spawnedList.Add(item);
            base.OnSpawned(item);
        }

        protected override void OnDespawned(Tile item)
        {
            item.ClearTile();
            _spawnedList.Remove(item);
            base.OnDespawned(item);
        }

        public void DespawnAll()
        {
            while(_spawnedList.Count != 0)
            {
                Despawn(_spawnedList[0]);
            }
        }
    }
}
