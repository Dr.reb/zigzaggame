﻿public class LevelCleaner
{
    private readonly Tile.Pool _pool;
    
    public LevelCleaner(Tile.Pool pool)
    {
        _pool = pool;
    }

    public void Clean()
    {
        _pool.DespawnAll();
    }
}
