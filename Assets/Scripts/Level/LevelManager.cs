﻿/// <summary>
/// Класс управления уровнем.
/// </summary>
public class LevelManager
{
    private readonly LevelCreator _creator;
    private readonly LevelCleaner _cleaner;
    private readonly Tile.Pool _pool;
    
    public LevelManager(LevelCreator creator, LevelCleaner cleaner, Tile.Pool pool)
    {
        _creator = creator;
        _cleaner = cleaner;
        _pool = pool;
    }

    public void SpawnTiles(int number)
    {
        _creator.SpawnBunchOfTiles(number);
    }

    public void ResetLevel()
    {
        _cleaner.Clean();
        _creator.SpawnStartZone();
    }

    /// <summary>
    /// Спавнит новый тайл, как только в пуле появляется тайл.
    /// </summary>
    public void UpdateLevel()
    {
        if (_pool.NumActive < _pool.NumTotal)
            SpawnTiles(1);
    }

}
