﻿using UnityEngine;

public class LevelCreator
{
    private readonly BonusBehaviour.Factory _bonusFactory;
    private readonly Tile.Pool _pool;

    private Vector3 _currentPosition = Vector3.zero;

    /// <summary>
    /// Координаты спавна первого тайла.
    /// </summary>
    private Vector3 _startTileSpawnPosition = new(-1, 0, -1);

    System.Random rnd = new();
    
    public LevelCreator(BonusBehaviour.Factory bonusFactory, Tile.Pool pool)
    {
        _bonusFactory = bonusFactory;
        _pool = pool;
    }

    public void SpawnBunchOfTiles(int num)
    {
        for (int i = 0; i < num; i++)
        {
            _currentPosition += rnd.Next(0, 2) == 0 ? Vector3.forward : Vector3.right;
            SpawnTile(_currentPosition);
        }
    }
    
    public void SpawnStartZone()
    {
        _currentPosition = Vector3.zero;
        SpawnTile(_startTileSpawnPosition, true);
    }
    
    /// <summary>
    /// Логика спавна тайла и прикрепления к нему бонуса.
    /// </summary>
    private void SpawnTile(Vector3 placeToSpawn, bool startTile = false)
    {
        Tile tile = _pool.Spawn();
        tile.transform.position = placeToSpawn;

        if(!startTile && rnd.Next(0, 2) == 0)
        {
            BonusBehaviour bonus = _bonusFactory.Create(BonusType.Standart);
            bonus.transform.position = tile.transform.position + Vector3.up * 0.2f;
            //bonus.transform.parent = tile.transform;
            tile.SetBonus(bonus);
        }
        else if (startTile)
        {
            tile.transform.localScale = new Vector3(3, 1, 3);
        }
    }
}


