﻿using UnityEngine;

public class AudioSystem
{
    private readonly AudioSource _source;

    public AudioSystem(AudioSource source)
    {
        _source = source;
    }

    public void PlayMusic()
    {
        _source.Play();
    }

    public void StopMusic()
    {
        _source.Stop();
    }

    public void Play(AudioClip clip)
    {
        const float volume = 1f;
        Play(clip, volume);
    }

    public void Play(AudioClip clip, float volume)
    {
        _source.PlayOneShot(clip, volume);
    }
}