using Zenject;
using UnityEngine;
using System;

public class ZigZagGameInstaller : MonoInstaller
{
    [Inject]
    private Settings _settings;

    public override void InstallBindings()
    {
        SignalsDeclaration();
        MainBindings();
        BonusBindings();
        LevelManagmentBindings();
        PlayerBindings();
        Container.Bind<InputHandler>().AsSingle();
        Container.Bind<AudioSystem>().AsSingle();
    }

    private void SignalsDeclaration()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<GameStateChanged>();
    }

    private void BonusBindings()
    {
        Container.Bind<ScoreSystem>().AsSingle().NonLazy();
        Container.BindFactory<BonusType, BonusBehaviour, BonusBehaviour.Factory>().FromFactory<BonusFactory>();
        Container.BindSignal<GameStateChanged>().ToMethod<ScoreSystem>(x => x.ResetScore).FromResolve();
    }

    private void LevelManagmentBindings()
    {
        Container.Bind<LevelCreator>().AsSingle();
        Container.Bind<LevelCleaner>().AsSingle();
        Container.Bind<LevelManager>().AsSingle();
        Container.BindMemoryPool<Tile, Tile.Pool>().WithInitialSize(100).FromComponentInNewPrefab(_settings.TilePrefab).UnderTransformGroup("Level");
    }

    private void MainBindings()
    {
        Container.BindInterfacesAndSelfTo<ZigZagGameController>().AsSingle().NonLazy();
        Container.BindFactory<GameStates, GameState, GameState.Factory>().FromFactory<GameStatesFactory>();
    }

    private void PlayerBindings()
    {
        Container.Bind<PlayerConditionChecker>().AsSingle();
        Container.Bind<Player>().AsSingle();
    }

    [Serializable]
    public class Settings
    {
        public GameObject TilePrefab;
    }
}