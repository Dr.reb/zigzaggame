﻿using UnityEngine;


/// <summary>
/// Класс, следящий за касаниями по экрану (и кликами мышкой в эдиторе)
/// </summary>
public class InputHandler 
{
    public bool Tap()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
            return true;
#endif
        if (Input.touchCount > 0)
        {
            if(Input.GetTouch(0).phase == TouchPhase.Began)
                return true;
        }

        return false;
    }
}
